package org.school.isc.soapexample.repository;

import com.school.soapexample.client.gen.Country;
import com.school.soapexample.client.gen.Currency;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
public class CountryRepository {

    private static final Map<String, Country> countries = new HashMap<>();

    @PostConstruct
    public void initData() {
        final Country russia = new Country();
        russia.setCapital("Moscow");
        russia.setName("Russia");
        russia.setCurrency(Currency.EUR);
        russia.setPopulation(140_000_000);
        countries.put("Russia", russia);


        final Country spain = new Country();
        spain.setCapital("Madrid");
        spain.setName("Spain");
        spain.setCurrency(Currency.EUR);
        spain.setPopulation(240_000_000);
        countries.put("Spain", spain);
        // initialize countries map
    }

    public Country findCountry(String name) {
        return countries.get(name);
    }
}